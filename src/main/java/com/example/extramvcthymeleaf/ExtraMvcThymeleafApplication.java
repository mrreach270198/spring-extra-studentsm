package com.example.extramvcthymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExtraMvcThymeleafApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExtraMvcThymeleafApplication.class, args);
    }

}
