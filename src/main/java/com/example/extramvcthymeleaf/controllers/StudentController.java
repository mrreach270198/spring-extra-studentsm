package com.example.extramvcthymeleaf.controllers;

import com.example.extramvcthymeleaf.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class StudentController {
    private StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("")
    public String viewIndex(Model model){
        model.addAttribute("studentList", studentService.getAll());
        return "index";
    }
}
