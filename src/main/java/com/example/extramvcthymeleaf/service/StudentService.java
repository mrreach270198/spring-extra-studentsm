package com.example.extramvcthymeleaf.service;

import com.example.extramvcthymeleaf.model.Student;

import java.util.List;

public interface StudentService {

    List<Student> getAll();
}
