package com.example.extramvcthymeleaf.service.imp;

import com.example.extramvcthymeleaf.model.Student;
import com.example.extramvcthymeleaf.repository.StudentRepository;
import com.example.extramvcthymeleaf.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImp implements StudentService {
    private StudentRepository studentRepository;

    @Autowired
    public StudentServiceImp(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public List<Student> getAll() {
        return studentRepository.getAllStudent();
    }
}
