package com.example.extramvcthymeleaf.repository;

import com.example.extramvcthymeleaf.model.Student;
import com.github.javafaker.Faker;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class StudentRepository {

    List<Student> studentList = new ArrayList<>();

    public StudentRepository() {
        Faker faker = new Faker();
        for (int i=1; i<=10; i++){
            Student student = new Student();
            student.setId(i);
            student.setName(faker.name().fullName());
            student.setGender("Male");
            student.setAddress(faker.address().streetAddress());
            studentList.add(student);
        }
    }

    public List<Student> getAllStudent(){
        return studentList;
    }
}
